## [3.2.3](https://gitlab.com/to-be-continuous/dbt/compare/3.2.2...3.2.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([f6abab6](https://gitlab.com/to-be-continuous/dbt/commit/f6abab6326b18b3dc701796eed7658e2be702488))

## [3.2.2](https://gitlab.com/to-be-continuous/dbt/compare/3.2.1...3.2.2) (2024-04-03)


### Bug Fixes

* **gcp:** use gcp-auth-provider's "latest" image tag ([ff1d792](https://gitlab.com/to-be-continuous/dbt/commit/ff1d792b8b816c0f8ea751b2671cbc976a089517))

## [3.2.1](https://gitlab.com/to-be-continuous/dbt/compare/3.2.0...3.2.1) (2024-1-30)


### Bug Fixes

* sanitize empty variable test expressions ([c3c9f79](https://gitlab.com/to-be-continuous/dbt/commit/c3c9f79805a0715542f3266492cdb843e75a107b))

# [3.2.0](https://gitlab.com/to-be-continuous/dbt/compare/3.1.1...3.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([19c0233](https://gitlab.com/to-be-continuous/dbt/commit/19c023313647b8a5c8370a1185c8bbc006350452))

## [3.1.1](https://gitlab.com/to-be-continuous/dbt/compare/3.1.0...3.1.1) (2023-12-14)


### Bug Fixes

* fix duplicate reference conflict in script ([f2eddbd](https://gitlab.com/to-be-continuous/dbt/commit/f2eddbd56da325dae4b31291c8221f8623018ba5))

# [3.1.0](https://gitlab.com/to-be-continuous/dbt/compare/3.0.1...3.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([e13440d](https://gitlab.com/to-be-continuous/dbt/commit/e13440dbff03bf4dee3d08a038e0009a8922ae33))

## [3.0.1](https://gitlab.com/to-be-continuous/dbt/compare/3.0.0...3.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([8fce1a0](https://gitlab.com/to-be-continuous/dbt/commit/8fce1a0ee744b6b3dd3c4335e93c17a54bd4ec66))

# [3.0.0](https://gitlab.com/to-be-continuous/dbt/compare/2.1.1...3.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([a5e7fb6](https://gitlab.com/to-be-continuous/dbt/commit/a5e7fb664897b5d093588ddca51c17f12d6d7ea8))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

## [2.1.1](https://gitlab.com/to-be-continuous/dbt/compare/2.1.0...2.1.1) (2023-08-01)


### Bug Fixes

* failure while decoding a secret [@url](https://gitlab.com/url)@ does not cause the job to fail (warning message) ([aa2281a](https://gitlab.com/to-be-continuous/dbt/commit/aa2281aaabd65c7c4bb73bbb76a6fceff1ece4f3))

# [2.1.0](https://gitlab.com/to-be-continuous/dbt/compare/2.0.0...2.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([773deb5](https://gitlab.com/to-be-continuous/dbt/commit/773deb505215adca39011f61c8d7d2afa01aaa1e))

# [2.0.0](https://gitlab.com/to-be-continuous/dbt/compare/1.3.0...2.0.0) (2023-04-05)


### Features

* **deploy:** redesign deployment strategy ([3d672cc](https://gitlab.com/to-be-continuous/dbt/commit/3d672ccb39cd94f1f3411240e60b0c167f11cd15))


### BREAKING CHANGES

* **deploy:** $AUTODEPLOY_TO_PROD no longer supported (replaced by $DBT_PROD_DEPLOY_STRATEGY - see doc)

# [1.3.0](https://gitlab.com/to-be-continuous/dbt/compare/1.2.0...1.3.0) (2023-03-21)


### Features

* improve logs when [@url](https://gitlab.com/url)@ variable eval fails ([58467fc](https://gitlab.com/to-be-continuous/dbt/commit/58467fc56572730c2d7c1d860e356c50618b3023))

# [1.2.0](https://gitlab.com/to-be-continuous/dbt/compare/1.1.1...1.2.0) (2023-01-28)


### Features

* **gcp:** add gcp-auth-provider variant ([441531a](https://gitlab.com/to-be-continuous/dbt/commit/441531a299b22ecce91c0c7cf9741c490f63f860))

## [1.1.1](https://gitlab.com/to-be-continuous/dbt/compare/1.1.0...1.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([1526977](https://gitlab.com/to-be-continuous/dbt/commit/1526977d16acceaec62c875da670dd2e64216121))

# [1.1.0](https://gitlab.com/to-be-continuous/dbt/compare/1.0.1...1.1.0) (2022-09-07)


### Features

* add SQLFluff for dbt project ([a91edfa](https://gitlab.com/to-be-continuous/dbt/commit/a91edfa6530060c30197fe19f53e9220bf7da2fe))

## [1.0.1](https://gitlab.com/to-be-continuous/dbt/compare/1.0.0...1.0.1) (2022-08-16)


### Bug Fixes

* add assert on required dbt_adapter ([d249a58](https://gitlab.com/to-be-continuous/dbt/commit/d249a58c9c67550221a48ed82b8050d3d58ce022))

# 1.0.0 (2022-08-16)


### Features

* initialize dbt template ([b37b79c](https://gitlab.com/to-be-continuous/dbt/commit/b37b79c5a97a191fcbfee77455bfb3c9dff69323))
